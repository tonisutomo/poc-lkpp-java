package models;

import java.util.List;

//import play.db.jdbc.BaseTable;
//import play.db.jdbc.CacheMerdeka;
//import play.db.jdbc.Id;
//import play.db.jdbc.Table;

import play.db.jpa.*;
import javax.persistence.*;

@Entity
@Table(name="barang")
public class Barang extends Model {

	//@Id(function = "nextval", sequence = "seq_barang")
	//public Long id;
	public String nama;
    public String deskripsi;
    public Float harga;
	public Integer stok;
	public Long user_id;
	
	public static List<Barang> getAll()
	{
		return find("NOT nama LIKE '?%' ORDER BY id desc").fetch();
	}
	
	public static List<Barang> getByUserId(Long user_id)
	{
		return find("user_id", user_id).fetch();
	}
	
	public User getUser(){
        if(user_id==null)
			return null;
		return User.findById(user_id);
    }

}
