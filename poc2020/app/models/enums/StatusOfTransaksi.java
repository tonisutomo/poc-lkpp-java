package models.enums;

//import play.db.jdbc.Enumerated;
import javax.persistence.EnumType;
import com.google.gson.annotations.SerializedName;

//@Enumerated(EnumType.STRING)
public enum StatusOfTransaksi
{
    PESAN("Pesan"), 
    KIRIM("Kirim"),
    TERIMA("Terima"),
    SELESAI("Selesai");

    public String label;

    private StatusOfTransaksi(String label)
    {
        this.label=label;
    }
}