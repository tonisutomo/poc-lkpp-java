package controllers;

import com.google.gson.Gson;
import models.enums.StatusOfTransaksi;
import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

public class Application extends BaseController {

    public static void index() {
        render();
    }
    public static void dashboard() {
        List<Barang> nBarang = Barang.findAll();
        List<Transaksi> transaksi = Transaksi.findAll();
        List<String> namaBarang = new ArrayList<String>();
        StatusOfTransaksi statusTransaksi [] = StatusOfTransaksi.values();
        for (int i = 0; i < transaksi.size(); i++) {
            namaBarang.add(transaksi.get(i).getNamaBarang());
        }
        List<Transaksi> kopi = Transaksi.find("status='1' and item_id=14").fetch();
        String transJson3 = new Gson().toJson(nBarang);
        String transJson4 = new Gson().toJson(transaksi);
        String transJson5 = new Gson().toJson(namaBarang);
        String transJson6 = new Gson().toJson(kopi);
        render(transJson3,transJson4,transJson5,transJson6);
    }
}