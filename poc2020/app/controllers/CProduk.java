package controllers;

import play.*;
import play.mvc.*;

import java.util.*;
import com.google.gson.Gson;

import models.Barang;
import models.Transaksi;
import models.enums.StatusOfTransaksi;

public class CProduk extends BaseController {

    public static void index() {
        if(loginInfo.get().id == null){
            redirect("/Application/index");
        }
        List<Barang> barangs = Barang.getAll();
        String transJson4 = new Gson().toJson(barangs);
        render(barangs, transJson4);
    }

    public static void transaksi() {
        if(loginInfo.get().id == null){
            redirect("/Application/index");
        }
        List<Transaksi> trans = new ArrayList<>();
        LoginInfo login = loginInfo.get();
        if(login.role_id == 2){
            trans = Transaksi.findAll();
        }else if(login.role_id == 3){
            trans = Transaksi.getByUserId(login.id);
        }else if(login.role_id == 4){
            trans = Transaksi.getByMerchantId(login.id);
        }
        String transJson = new Gson().toJson(trans);
        render(trans, transJson);
    }

    public static void form(Long id){
        Barang barang = new Barang();
        if(id != null) barang = Barang.findById(id);
        renderArgs.put("barang",barang);
        renderTemplate("CProduk/form.html");
    }

    public void formSubmit(Transaksi trx, Float price, Float total_price, String pengiriman){
        Barang barang = new Barang();
        if (trx.item_id != null){
            barang = Barang.findById(trx.item_id);
            if ( barang.stok < trx.jumlah_item)
            {
                flash.error("Pesanan melebihi Stok");
                flash.put("jumlah_item",trx.jumlah_item);
                flash.put("alamat",trx.alamat);
                form(barang.id);
            }
            barang.stok = barang.stok - trx.jumlah_item;
            barang.save();
        }

        System.out.println("Price " + price);
        System.out.println("Total Price " + total_price);
        System.out.println("Trx harga " + trx.harga_item);
        System.out.println("Trx jumlah " + trx.jumlah_item);
        System.out.println("Trx total " + trx.total_harga);
        System.out.println("Pengiriman " + pengiriman);

        trx.tanggal = new Date();
        trx.status = StatusOfTransaksi.PESAN;
        trx.harga_item = price;
        trx.total_harga = total_price;
        trx.pengiriman = pengiriman;
        trx.save();
        flash.success("Pesanan berhasil dipesan");
        redirect("/CProduk/transaksi");
    }

    public void proses(Long id, StatusOfTransaksi status){
        Transaksi trx = Transaksi.findById(id);
        trx.status = status;
        trx.save();
        redirect("/CProduk/transaksi");
    }

    public void batalPesan(Long id){
        Transaksi trx = Transaksi.findById(id);
        int jumlah_item = trx.jumlah_item;
        Long item_id = trx.item_id;
        trx.delete();

        Barang barang = new Barang();
        barang = Barang.findById(item_id);
        int stok = barang.stok;
        int total = stok + jumlah_item;
        barang.stok = total;
        barang.save();
        flash.success("Berhasil membatalkan Pembelian");
        redirect("/CProduk/transaksi");
    }


}