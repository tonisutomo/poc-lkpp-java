/*
 Navicat Premium Data Transfer

 Source Server         : Local Postgres
 Source Server Type    : PostgreSQL
 Source Server Version : 100011
 Source Host           : localhost:5433
 Source Catalog        : poc-2020
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 100011
 File Encoding         : 65001

 Date: 09/02/2020 22:09:40
*/


-- ----------------------------
-- Table structure for transaksi
-- ----------------------------
DROP TABLE IF EXISTS "transaksi";
CREATE TABLE "transaksi" (
  "id" int8 NOT NULL DEFAULT NULL,
  "user_id" int8 DEFAULT NULL,
  "item_id" int8 DEFAULT NULL,
  "jumlah_item" int8 DEFAULT NULL,
  "harga_item" float8 DEFAULT NULL,
  "total_harga" float8 DEFAULT NULL,
  "alamat" varchar(255) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "status" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "tanggal" timestamp(6) DEFAULT NULL::timestamp without time zone,
  "merchant_id" int8 DEFAULT NULL,
  "pengiriman" varchar(255) COLLATE "pg_catalog"."default" DEFAULT NULL
)
;
ALTER TABLE "transaksi" OWNER TO "postgres";

-- ----------------------------
-- Records of transaksi
-- ----------------------------
BEGIN;
INSERT INTO "transaksi" VALUES (1, 2, 2, 10, 5000000, 50000000, 'Pasuruan', '2', '2020-01-30 22:44:06.144', 3, 'JNE');
INSERT INTO "transaksi" VALUES (7, 2, 1, 4, 100000, 400000, 'pasuruan', '2', '2020-01-11 15:24:45.118', 3, 'JNE');
COMMIT;

-- ----------------------------
-- Primary Key structure for table transaksi
-- ----------------------------
ALTER TABLE "transaksi" ADD CONSTRAINT "transaksi_pkey" PRIMARY KEY ("id");
