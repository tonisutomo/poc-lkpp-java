package models.jcommon.mail;

import java.util.Date;

import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.persistence.EnumType;

import models.jcommon.config.Configuration;
import models.jcommon.db.base.BaseDomainObject;
import models.jcommon.db.base.JdbcUtil;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;

import play.Logger;
import play.Play;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

@Table(name="MAIL_QUEUE")
public  class MailQueue extends BaseDomainObject {
	

	public static final String ENGINE_VERSION="4.0.0"; 
	//setting sub_category di Configuration
	public static final String SMTP_HOST = "mail.smtp.host"; 
	public static final String SMTP_SENDER= "mail.sender";
	public static final String SMTP_USER= "mail.user";
	public static final String SMTP_RETRY = "mail.retry";    
	public static final String SMTP_DELAY = "mail.delay";    
	public static final String SMTP_PORT = "mail.port";		 
	public static final String SMTP_SENDER_PASWORD = "mail.smtp.password";
	public static final String SMTP_BCC = "mail.recipient.bcc"; 
	public static final String SMTP_MAIL_TEST = "mail.test"; 
	//[ANDIK] TODO Harusnya Enum ini ada di SPSE karena jcommon dipakai oleh aplikasi lain (tidak relevan).
	@Deprecated
	public static enum JENIS {
		REGISTRASI_PENYEDIA(1),
		PENGIRIMAN_PENAWARAN (2),
		PENGUMUMAN_PRA(3),
		DOK_KUALIFIKASI(4),
		PEMBATALAN_PENGAKTIFAN_LELANG(5),
		PENGUMUMAN_PEMENANG(6),
		UNDANGAN_PL(7),
		UNDANGAN_PEMBUKTIAN(8),
		UNDANGAN_VERIFIKASI(9),
		UNDANGAN_KONTRAK(10),
		PENGUMUMAN_ADENDUM(11);
		public final int id;
		private JENIS(int id) {
			this.id=id;
		}
	}
	
	public MailQueue()
	{
		prioritas=MAIL_PRIORITAS.MEDIUM;
	}
	
	@Enumerated(EnumType.ORDINAL)
	public enum MAIL_PRIORITAS
	{
		HIGH, MEDIUM, LOW;
	}
	
	/**Status dari email */
	@Enumerated(EnumType.ORDINAL)
	public static enum MAIL_STATUS {
		/* 
		 * Menurut definisi di database=> 
		 * 	0: OUTBOX (dikirim tapi blm berhasil)
		 * 	1: SENT
		 * 	2: FAILED (gagal, sudah di-retry max)
		 * 	3: INBOX (baru masuk, belum dikirim)
		 * 	4: SENDING artinya sedang diproses kirim oleh SMTP. Status ini cuma sebentar
		 * 		jika SMTP gagal, segera diubah menjadi outbox (atau failed jika retry sdh max)
		 * 		jika SMTP sukses, diubah jadi sent.
		 * 		Status ini baru ada di versi 4.0
		 */
		OUTBOX(0), SENT(1), FAILED(2), INBOX(3), SENDING(4);
		private int value;

		MAIL_STATUS(int value) {
			this.value = value;
		}

		int getValue() {
			return this.value;
		}
	}
	@Enumerated(EnumType.ORDINAL)
	public static enum BACA_STATUS{
		READ,
		UNREAD
	}
	
	@Id(sequence="seq_mail_queue", function="nextsequence")	
	public Long id;

	public String from_address;

	public String to_addresses;

	public String cc_addresses;

	public String bcc_addresses;

	public String subject;

	public String body;

	public Integer mime = Integer.valueOf(0);

	public Integer retry;

	public String exception;

	public Date enqueue_date;

	public Date send_date;

	public MAIL_STATUS status;

	public String noemail;

	public Integer jenis = Integer.valueOf(1);
	
	public BACA_STATUS bcstat = BACA_STATUS.UNREAD;
	
	/**Kolom ini digunakan untuk keperluan macem-macem, terserah.
	 * Kalau di SPSE 3,5 untuk llg_id, rkn_id, dan pnt_id
	 *  
	 * */
	public Long ref_1;
	public Long ref_2;
	public Long ref_3;
	
	/**Kolom ini digunakan untuk keperluan macem-macem, terserah.
	 * Bisa diisi jenis penerima email: PNT, PNY, PPK, dst.  */
	public String ref_a;
	public String ref_b;
	public String ref_c;
	
	/**Kedua field ini hanya untuk kompatibilitas dengan SPSE 3.5 dan 
	 * menjaga agar tidak perlu ada migrasi data
	 * Sebenarnya ini menyalahi prinsip bahwa jcommon harus independen terhadap aplikasi utama.
	 */
	public Long lls_id;
	public Long rkn_id;
	
	
	/* versi dari email engine.
	 * Ini berguna ketika ada migrasi dari engine lama 
	 */
	public String engine_version;
	
	/** Prioritas email. Angka terkecil akan dikirim lebih dulu */
	public MAIL_PRIORITAS prioritas;
	
	public String toString()
	{
		return String.format("[%s TO %s] %s  - %s", id, to_addresses, subject, status);
	}
	
	
	/**Method ini digunakan untuk 'clean up' terhadap status MailQueue.
	 * Status MailQueue mungkin tidak akurat karena proses shutdown aplikasi ketika email sedang dalam
	 * proses pengiriman
	 */
	@Deprecated //[ANDIK] tidak diperlukan 
	public static void cleanUp()
	{
		//1. mail yg berstatus SENDING harus diubah menjadi OUTBOX
		int result = Query.update("update mail_queue set status=? where status=?", MAIL_STATUS.OUTBOX, MAIL_STATUS.SENDING);
		if(result>0)
			Logger.debug("MailQueue.cleanUp. Set status from SENDING to OUTBOX %s", result);
	}
	
		
		/**Save this object and force to commit immediately using JDBC
		 * It is useful in long-running job using this Model
		 * 
		 *  
		 *  */
		public void saveViaJDBC() {
			//save via JDBC
			JdbcUtil.executeUpdateUsingJDBC("UPDATE MAIL_QUEUE SET retry=?, exception=?, send_date=?, status=?, auditupdate=?, audituser='MAIL-JOB' where id=?", 
					retry, exception, send_date, status.ordinal(), send_date, id);
			
		}
		
		public void prePersist()
		{
			if(StringUtils.isEmpty(to_addresses))
				throw new IllegalArgumentException("TO Addresses is null");
		}
	
		/**Mendapatkan body email dalam bentuk asli (bukan Base64) serta membuang email header
		 * 
		 * Contoh body:
From: helpdesk@lpse.kab.sigi.go.id
To: a.rekanan1@pengadaannasional-bappenas.go.id
Message-ID: &lt;15035833.47.1365062242073.JavaMail.root@cluster1&gt;
Subject: (LPSE) Pengumuman Pemenang Lelang
MIME-Version: 1.0
Content-Type: text/html; charset=us-ascii
Content-Transfer-Encoding: 7bit
8231047100123529020<br>Kepada Yth.<br>
Pemilik email <strong>a.rekanan1@pengadaannasional-bappenas.go.id</strong><br>
di<br>Tempat

		 * 
		 * @return
		 */
		public String getRawBody()
		{
			if(StringUtils.isEmpty(body))
				return null;
			String str = null;
			if(StringUtils.isEmpty(engine_version))
				str=new String(Base64.decodeBase64(body));
			else 
				str=new String(Base64.decodeBase64(Base64.decodeBase64(body)));
//			int pos=str.indexOf("<br");
//			if(pos>0)
//				str=str.substring(pos);
			return str;
		}
		
		
		/**Dapatkan MailQueue berikutnya dengan ketentuan
		 * 1. retry < mail.retry.count (artinya msh dalam proses retry)
		 * 2. status outbox (blm terkirim)
		 * 3. id > current_mail_id
		 * 4. yg dikirim (tapi gagal) bbrp waktu yang lalu (sesuai CONFIG.mail.delay)
		 * 
		 * @param current_mail_id
		 * @return
		 */
		public static MailQueue getNextQueue(long current_mail_id) {
		
			int retryCount=getRetryCount();
			int delayDuration=getDelayDuration();
			String sql="(status=? or status= ?) and id > ? and retry <= ? and (send_date is null or (send_date + interval '" +   delayDuration + " seconds' <= ?)) order by prioritas, id";			
			return find(sql, MAIL_STATUS.OUTBOX, MAIL_STATUS.INBOX, current_mail_id, retryCount, new Date()).first();
		}
		

		public static int getRetryCount() {
			return Configuration.getInt(SMTP_RETRY);
		}
		
		/**Get delay time before retry (in seconds) */
		public static int getDelayDuration() {
			return Configuration.getInt(SMTP_DELAY);
		}
}