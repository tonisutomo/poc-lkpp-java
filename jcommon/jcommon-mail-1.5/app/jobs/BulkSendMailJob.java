package jobs;

import models.jcommon.config.Configuration;
import models.jcommon.mail.MailQueue;
import models.jcommon.util.DateUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.StopWatch;
import play.Logger;
import play.Play;
import play.cache.Cache;
import play.db.jpa.NoTransaction;
import play.jobs.Job;
import play.jobs.OnApplicationStart;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@OnApplicationStart(async=true)
@NoTransaction
public class BulkSendMailJob extends Job {

    static final int BATCH = Integer.parseInt(Play.configuration.getProperty("mail.max-per-connection","20")); // defautl batch mail per connection

    private Properties mailProperties;
    private String testMail;
    private Session session; // mail session
    private String mailSender; // mail sender LPSE: bisa berupa "Andik Yulianto"<andik@lkpp.go.id>
    private String userSmtp;//beda dengan mail sender, tidak boleh ada Nama Orang. Contoh benar: andik@lkpp.go.id
    private int retryCount;
    private int delayDuration; // in seconds
    private String password;
    private static boolean firstRun=true;
    private static int passCount=0;
    private String savedMail;

    /**Dijalankan hanya satu kali
     *
     */
    private void setup() {
        //simpan di Cache
    	String smtpHost="";
        mailProperties = Cache.get("mailProperties", Properties.class);
        if(mailProperties==null)
        {
        	smtpHost=Configuration.getConfigurationValue(MailQueue.SMTP_HOST,"");
            mailProperties=new Properties();
            mailProperties.put("mail.smtp.host", 
            		smtpHost);
            mailProperties.put("mail.smtp.port", Configuration.getConfigurationValue(MailQueue.SMTP_PORT,""));
            testMail=Configuration.getConfigurationValue(MailQueue.SMTP_MAIL_TEST);
            mailProperties.put("mail.debug", "false");
            mailProperties.put("mail.smtp.auth", "true");
            mailProperties.put("mail.transport.protocol", "smtp");
            mailProperties.put("mail.smtp.starttls.enable", "true");
            mailProperties.put("mail.smtp.ssl.trust", Configuration.getConfigurationValue(MailQueue.SMTP_HOST));
            mailSender = Configuration.getConfigurationValue(MailQueue.SMTP_SENDER);
            mailProperties.put("mailSender", mailSender);
            /*Jika ada mail.smtp.user maka itu dipakai sebagai user
             * jika tidak ada maka pakai mail.sender sebagai akun
             */
            //dapatkan smtpUser untuk kasus: "Andik Yulianto"<andik@lkpp.go.id>
            Pattern p=Pattern.compile("\".+\"<(.+)>");
            Matcher m=p.matcher(mailSender);
            if(!m.matches())
                userSmtp=mailSender;
            else
                userSmtp=m.group(1);
            String mailUser=Configuration.getConfigurationValue("mail.smtp.user");
            if(StringUtils.isNotEmpty(mailUser))
            	userSmtp=mailUser;
            mailProperties.put("userSmtp", userSmtp);
            mailProperties.put("password", Configuration.getConfigurationValue(MailQueue.SMTP_SENDER_PASWORD, ""));
            mailProperties.put("retryCount", Configuration.getConfigurationValue(MailQueue.SMTP_RETRY, "3"));
            mailProperties.put("delayDuration", Configuration.getConfigurationValue(MailQueue.SMTP_DELAY, "10"));
            Cache.set("mailProperties", mailProperties, "1h");
        }

        retryCount = Integer.parseInt(mailProperties.getProperty("retryCount"));
        delayDuration = Integer.parseInt(mailProperties.getProperty("delayDuration"));
        password=mailProperties.getProperty("password");
        userSmtp=mailProperties.getProperty("userSmtp");
        mailSender=mailProperties.getProperty("mailSender");

        session = Session.getInstance(mailProperties, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(userSmtp, password);
            }
        });

        if(firstRun) {//hanya jalan sekali
            try {
                session.getTransport().connect();
                Logger.info("SMTP Server status is OK: %s", smtpHost);
            } catch (MessagingException e) {
                Logger.error("[%s] Error connecting to SMTP Server: %s", Configuration.getConfigurationValue(MailQueue.SMTP_HOST), e);
            }
        }
    }

    @Override
    public void doJob() {
        /* In Development OR production,
         * mail sending can be disabled by adding in application launcher
         * -Dmail.sending.disabled=true
         * Di production dipakai saat aplikasi running READONLY (standby server)
         * application.conf -> mode.readonly=true
         */
        if("true".equals(System.getProperty("mail.sending.disabled")) 
        		|| "true".equals(Play.configuration.getProperty("mode.readonly")))
        {
            Logger.info("[MAIL-JOB DISABLED] MailSenderJob  all mails are not sent ");
            return;
        }
        setup();
        if(firstRun)
        {
            Logger.info("[MAIL-JOB] started. Retry: %s times, Delay: %s seconds", retryCount, delayDuration );
            if(Play.mode== Play.Mode.DEV)
                Logger.info("[MAIL-JOB] This job can be disabled in DEV mode by adding '-Dmail.sending.disabled=true' in Application Launcher", retryCount, delayDuration );
        }
        firstRun=false;
        try {
            long count = MailQueue.count("status IN (0,2,3) ");
            if(count > 0) {
                session.getTransport().connect();
//                Logger.debug("SMTP Server status is OK");
                StopWatch sw=new StopWatch();
                sw.start();
                int page = (int) count / BATCH;
                int i = 0;
                passCount = 0;
                while (i <= page) {
                    List<MailQueue> list = MailQueue.find("status IN (0,2,3) ORDER BY id").fetch(i, BATCH);
                    sendBulkEmail(list);
                    i++;
                }
                sw.stop();
//                Logger.debug("BulkSendMailJob, Done, processed email: %,d, duration: %s", passCount, sw);
            }
        } catch (Exception e) {
//            Logger.error("[Error connecting to SMTP Server or sending emails", e);
        }
        //jalankan job pada sekian detik berikutnya
        new BulkSendMailJob().in(delayDuration + "s");
    }

    public void sendBulkEmail(List<MailQueue> list) {
        setup();
        if(CollectionUtils.isEmpty(list))
            return;
        Transport transport = null;
        try {
            Iterator<MailQueue> iterator = list.iterator();
            while (iterator.hasNext()){
                MailQueue mq = iterator.next();
                if (mq == null)
                    continue;
                int retry = 0;
                if (mq.retry != null) {
                    retry = mq.retry.intValue();
                }
                retry++;

                if (retry > retryCount) {
                    continue;
                }
                try {
                    // Check transport connection first...
                    if (transport == null || !transport.isConnected()) {
                        if (transport != null) {
                            try {
                                transport.close();
                            } catch (Exception ex) {
                                // Ignore - we're reconnecting anyway
                            }
                            transport = null;
                        }
                        transport = session.getTransport();
                        transport.connect();
                    }
                    // SELALU GUNAKAN TANGGAL RIIL (bukan simulasi)
                    mq.send_date = DateUtil.newDate(); // meskipun gagal dikirim, send_date tetap diisi
                    mq.audituser = "MAIL-JOB";
                    // create the messge.
                    MimeMessage mimeMessage = new MimeMessage(session);
                    mimeMessage.setFrom(new InternetAddress(userSmtp));
                    MimeMultipart rootMixedMultipart = new MimeMultipart("mixed");
                    mimeMessage.setContent(rootMixedMultipart);

                    MimeMultipart nestedRelatedMultipart = new MimeMultipart("related");
                    MimeBodyPart relatedBodyPart = new MimeBodyPart();
                    relatedBodyPart.setContent(nestedRelatedMultipart);
                    rootMixedMultipart.addBodyPart(relatedBodyPart);

                    MimeMultipart messageBody = new MimeMultipart("alternative");
                    MimeBodyPart bodyPart = null;
                    for (int i = 0; i < nestedRelatedMultipart.getCount(); i++) {
                        BodyPart bp = nestedRelatedMultipart.getBodyPart(i);
                        if (bp.getFileName() == null) {
                            bodyPart = (MimeBodyPart) bp;
                        }
                    }
                    if (bodyPart == null) {
                        MimeBodyPart mimeBodyPart = new MimeBodyPart();
                        nestedRelatedMultipart.addBodyPart(mimeBodyPart);
                        bodyPart = mimeBodyPart;
                    }
                    bodyPart.setContent(messageBody, "text/alternative");

                    // Create the HTML text part of the message.
                    MimeBodyPart htmlTextPart = new MimeBodyPart();
                    htmlTextPart.setContent(mq.getRawBody(), "text/html;charset=UTF-8");
                    messageBody.addBodyPart(htmlTextPart);

                    mimeMessage.setFrom(new InternetAddress(mq.from_address));
                    String recipient = mq.to_addresses;
                    // simulation testmail maybe more than 1
                    if (testMail != null && Play.mode.equals(Play.Mode.DEV)) {
                        String[] ary = testMail.split(";");
                        for (String to : ary)
                            mimeMessage.addRecipients(Message.RecipientType.TO, to.toString());
                        mimeMessage.setSubject("[TO: " + mq.to_addresses + "]" + mq.subject);
                        recipient = recipient + "#" + testMail;
                    } else {
                        mimeMessage.setRecipient(Message.RecipientType.TO, new InternetAddress(mq.to_addresses));
                        mimeMessage.setSubject(mq.subject);
                    }
                    mimeMessage.setSentDate(new Date());
                    mimeMessage.saveChanges();
                    if (mimeMessage.getMessageID() != null) {
                        // Preserve explicitly specified message id...
                        mimeMessage.setHeader("Message-ID", mimeMessage.getMessageID());
                    }
                    transport.sendMessage(mimeMessage, mimeMessage.getAllRecipients());

                    // DEVELOPMENT, pada mode development, tidak usah kirim tapi sleep beberapa saat
                    mq.status = MailQueue.MAIL_STATUS.SENT; //sukses
                    mq.exception = null; //update status as SENT
                    passCount++;
                    Logger.debug("[MAIL-JOB] MailSent [%s to %s] '%s'", mq.id, mq.to_addresses, mq.subject);
                } catch (Exception e) {
                    // eror: java.net.UnknownHostException, javax.mail.SendFailedException, java.net.ConnectException
                    mq.exception = e.toString();
//                    Logger.error(e, "[MAIL-JOB] Error sending mail, subject: %s, %s", mq, mq.exception);
                    mq.retry = retry;
                    // jika gagal,status menjadi OUTBOX atau FAILED
                    if (mq.retry >= retryCount)
                        mq.status = MailQueue.MAIL_STATUS.FAILED;
                    else
                        mq.status = MailQueue.MAIL_STATUS.OUTBOX;
                    if (mq.exception.contains("java.net.ConnectException")) {

                    }
                }
                mq.save();
                iterator.remove();
            }
        }finally {
            try {
                if (transport != null) {
                    transport.close();
                }
            }catch (Exception ex) {
                Logger.error(ex,"Failed to close server connection after message failures");
            }
        }
    }
}
