package models.workflow.instance;


public class VariableInstance  {
	
	public String variable_name;
	public String value;
	
	public VariableInstance(String variableName) {
		this.variable_name=variableName;
	}

	/** Convert to boolean if possible */
	public boolean asBoolean()
	{
		return Boolean.parseBoolean(value);
	}
	
	public String toString()
	{
		return variable_name+"=" + value;
	}
	
}