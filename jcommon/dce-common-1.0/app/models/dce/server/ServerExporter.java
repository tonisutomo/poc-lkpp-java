package models.dce.server;


import java.sql.SQLException;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.time.StopWatch;

import models.dce.db.Exporter;
import models.dce.db.SqlManipulationSelect;
import models.jcommon.db.base.JdbcUtil;
import models.jcommon.util.TempFileManager;

/**Exported ini akan memproduksi data CSV dengan kolom sbb
 * 	MD5_ALL_COLUMNS, PK1, PK2, ....
 *  
 *  data ini disimpan di file $temporaryFileMD5 dan di database H2  pada tabel $temporaryTableNameMD5
 *  
 *  Selanjutnya dihitung MD5AllRows yang berisi md5 dari seluruh row (khusus kolom MD5_ALL_COLUMNS)
 *  
 * @author Mr. Andik
 *
 */
public class ServerExporter extends Exporter {

	
	
	public ServerExporter(String sessionId, String query, boolean containsSubQuery, String primaryKey) throws SQLException {
		super(sessionId, query, primaryKey, containsSubQuery);		
	}
	

	

	@Override
	protected void init() {
		temporaryTableNameMD5 ="T_MD5_SERVER_" + Math.abs(sessionId.hashCode());
		super.init();
	}




	/**Execute the query to generate MD5 data for each row
	 * 
	 * @param query
	 * @param primaryKey
	 * @throws SQLException 
	 */
	public void execute() throws SQLException
	{
		cleanH2Database(); 
		exportMD5Info();
	}

	
	@Override
	protected String getH2DatabaseName() {
		return TempFileManager.baseFolder + "/h2/dce-server";
	}


	@Override
	public boolean isIncludeAllColumns() {
		return false;
	}


	

}
