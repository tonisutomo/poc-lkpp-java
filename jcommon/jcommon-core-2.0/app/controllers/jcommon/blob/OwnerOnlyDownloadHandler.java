package controllers.jcommon.blob;

import models.jcommon.blob.BlobTable;
import play.mvc.Scope.Session;

/**Hanya owner yang boleh melakukan download; 
 * dicek antara audituser dengan userId yang login.
 * 
 * INI TIDAK BERLAKU untuk group seperti dokumen lelang yang di-create oleh user A
 * tapi di-download oleh user B dimana keduanya dalam satu kepanitiaan.
 * 
 * @author I Wayan Wiprayoga W
 */
public class OwnerOnlyDownloadHandler implements DownloadSecurityHandler {
	/**
	 * TODO: Seharusnya semua yang boleh download file didefinisikan disini
	 * @param secureIdBlobTable objek {@link BlobTable}
	 * @return nilai true memperbolehkan download
	 */
	@Override
	public boolean allowDownload(BlobTable blob) {
		String blobOwner=blob.audituser;
		String currentUser=Session.current().get("userId");
		if(currentUser==null)
			return false;
		return currentUser.equals(blobOwner);
	}
}
