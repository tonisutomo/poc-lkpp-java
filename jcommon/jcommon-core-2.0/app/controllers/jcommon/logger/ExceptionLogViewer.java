package controllers.jcommon.logger;

import play.mvc.Controller;

public class ExceptionLogViewer extends Controller {

	public static void viewException(String exceptionId)
	{
		exceptionId="@" + exceptionId;
		render(exceptionId);
	}
}
