package controllers.jcommon.cdn;

import java.util.concurrent.ExecutionException;

import play.Logger;
import play.libs.WS;
import play.libs.WS.WSRequest;
import play.mvc.Controller;

public class CDNClientCtr extends Controller{

	/**CDN Server calls this action to inform that CDN Client should download
	 * the file from CDN Server
	 * @param uploadTicket
	 * @throws ExecutionException 
	 * @throws InterruptedException 
	 */
	public static void cdnUploadCompleted(String urlToDownload, String uploadTicket, String md5Hash) throws InterruptedException, ExecutionException
	{
		String url=urlToDownload + "/" + uploadTicket;
		WSRequest req= WS.url(url).timeout("5min");
		play.libs.WS.HttpResponse resp= req.getAsync().get();
		String status=resp.getStatusText();
		Logger.debug("Download file from URL: %s, MD5: %s, status: %s", url, md5Hash, status);
	}
}
