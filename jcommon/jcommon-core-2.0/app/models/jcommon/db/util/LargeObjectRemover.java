package models.jcommon.db.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import models.jcommon.util.ObjectMonitor;

import org.apache.commons.io.IOUtils;
import org.postgresql.jdbc.PgConnection;
import org.postgresql.largeobject.LargeObject;
import org.postgresql.largeobject.LargeObjectManager;

import play.Logger;
import play.Play;


/** List and remove large object
 *  */
public class LargeObjectRemover {

	private PrintStream log;
	private ObjectMonitor objectMonitor;
	public LargeObjectRemover(PrintStream out, ObjectMonitor objectMonitor) {
		this.log=out;
		this.objectMonitor=objectMonitor;
	}

	private void notifyMonitor(String message)
	{
		synchronized (objectMonitor) {
			objectMonitor.notifyAll();
			objectMonitor.status=message;
		}
	}
	
	private void log(String format, Object ... values)
	{
	
		Logger.info("[LargeObject] " + format, values);
		log.append(String.format(format, values));
		log.append("\n");
	}
	/**
	 * 
	 * @param deleteLOB
	 * @param pathToSaveLOB
	 * @return false if disc capacity is not enought
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 * @throws IOException
	 */
	public String execute(boolean deleteLOB, String pathToSaveLOB) throws ClassNotFoundException, SQLException, IOException
	{
		String url=Play.configuration.getProperty("db.url");
		String user=Play.configuration.getProperty("db.user");
		String pass=Play.configuration.getProperty("db.pass");
		Class.forName("org.postgresql.Driver");
		//harus langsung gunakan postgre
		PgConnection conn=(PgConnection) DriverManager.getConnection(url, user, pass);
		log("***** Move LargeObject (if exists) from Database into File **********");
		log("Connection: %s, files saved to %s ", conn.getClass(), pathToSaveLOB);
		notifyMonitor("Menghitung jumlah LargeObject");
		LargeObjectManager lom=conn.getLargeObjectAPI();
		
		Statement st=conn.createStatement();
		conn.setAutoCommit(false);
		 ResultSet rs= st.executeQuery("select distinct(loid) from pg_largeobject ");
		 int row=0;
		 long totalSize=0;
		 //first step is to count size of all blobs
		 List<Long> listOID=new ArrayList<Long>();
		 while(rs.next())
		 {
			 try
			 {
				 Long oid=rs.getLong(1);
				 listOID.add(oid);
				 LargeObject lo= lom.open(oid);
				 int size=lo.size();
				 totalSize+=size;
				 lo.close();
				 row++;
			 }
			 catch(Exception e)
			 {
				 Logger.error(e.toString());
			 }
		 }
		 
		 log("LargeObject counts %,d, total size %,d bytes", row, totalSize);
		 notifyMonitor(String.format("LargeObject berjumlah: %,d, ukuran: %,d", row, totalSize));
		 int totalCount=row;
		 row=0;
		 //check if hard disc capacity is enough
		 File lobStorage=new File(pathToSaveLOB);
		 long freeSpace=lobStorage.getFreeSpace();
		 long required=totalSize+100000;
		 if(freeSpace<required)
		 {
			 String str=String.format("Space Hardisk tidak cukup:  %s. Diperlukan: %,d", pathToSaveLOB, required);
			 log(str);
			 notifyMonitor(str);
			 return str;
		 }
			 //now ACTUAL DELETE
		 for(Long oid: listOID)
		 {
			 LargeObject lo= lom.open(oid);
			 int size=lo.size();
			 row++;
			 File file=new File(pathToSaveLOB + "/oid-" + oid);
			 file.getParentFile().mkdirs();
		
			 String copyOrMove;
			 if(deleteLOB)
				 copyOrMove="Move";
			 else
				 copyOrMove="Copy";
			 log("%s. %s blob to file: %s, %,d bytes ", row, copyOrMove, file,size);
			 notifyMonitor(String.format("Memproses LargeObject %,d dari %,d", row, totalCount));
			 //do copy
			 OutputStream out=new FileOutputStream(file);
			 IOUtils.copy(lo.getInputStream(), out);
			 out.flush();
			 lo.getInputStream().close();
		
			 if(deleteLOB)
			 {
				 lom.delete(oid);
				 conn.commit();
			 }
			
		 }
		 
		 rs.close();
		 st.close();
		 log("Objects Count: %s, size: %,d ", row, totalSize);
		 notifyMonitor(String.format("SELESAI, LargeObject yang diproses %,d - %,d bytes - disimpan di %s", totalCount, totalSize, pathToSaveLOB));
		 objectMonitor.running=false;
		 return null;
	}
	
	public void main(String[] args)
	{
		ObjectMonitor om=new ObjectMonitor();
		LargeObjectRemover lom=new LargeObjectRemover(System.out, om);
		
	}
	
	
}
