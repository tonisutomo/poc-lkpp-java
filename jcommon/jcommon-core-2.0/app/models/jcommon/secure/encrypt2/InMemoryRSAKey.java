package models.jcommon.secure.encrypt2;

import java.io.IOException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.apache.commons.codec.binary.Base64;

import models.jcommon.secure.encrypt.CipherEngineException;

/**Class ini berfungsi untuk generate dan menyimpan key RSA yang digunakan di CipherEngine2
 * Key-key yang digunakan digenerate ketika aplikasi pertama kali STARTED (OnApplicationStart) 
 * @author idoej
 */
public class InMemoryRSAKey {
	public PublicKey publicKey;
	public PrivateKey privateKey;
	public String idxKey;
	private static final Map<String, InMemoryRSAKey> generatedKeys = new HashMap<>();	
	private static final int MAX_SIZE = 100;
	public static final int DEFAULT_SIZE = 10;
	
	/**
	 * Constructor dengan generate private/public key di memory dengan index = idxKey
	 * @param idxKey index di list
	 * @throws CipherEngineException
	 */
	private InMemoryRSAKey(String idxKey) throws CipherEngineException {
		this.generateKeyPair();
		this.idxKey = idxKey;
	}

	
	/** 
	 * Generate in memory public/private key 
	 * @throws CipherEngineException 
	 * @throws IOException 
	 * @throws InvalidKeySpecException 
	 * @throws NoSuchProviderException */
	private void generateKeyPair() throws CipherEngineException {
		try {
			KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
			kpg.initialize(2048);
			KeyPair kp= kpg.generateKeyPair();

			this.privateKey = kp.getPrivate();
			this.publicKey = kp.getPublic();
		} catch(Exception e) {
			throw new CipherEngineException(e);
		}
	}
	
	/**
	 * Method untuk mengambil public key dari map key yang ada berdasarkan index
	 * @param idxKey index key dalam map
	 * @return public key dalam format base 64
	 */
	public static String getPublic(String idxKey) {
		return generatedKeys.get(idxKey).getPublicKeyEncoded();
	}
	
	/**
	 * Method untuk mengambil private key dari map key yang ada berdasarkan index
	 * @param idxKey index key dalam map
	 * @return private key dalam format base 64
	 */
	public static String getPrivate(String idxKey) {
		return generatedKeys.get(idxKey).getPrivateKeyEncoded();
	}
	
	/**
	 * get base 64 encoded public key
	 * @return
	 */
	public String getPublicKeyEncoded() {
		return Base64.encodeBase64String(this.publicKey.getEncoded());
	}
	
	/**
	 * get base 64 encoded private key
	 * @return
	 */
	public String getPrivateKeyEncoded() {
		return Base64.encodeBase64String(this.privateKey.getEncoded());
	}
	
	/**
	 * Generate keys yang digunakan untuk proses enkripsi supaya siap pakai. Jumlah key yang digenerate sebanyak DEFAULT_SIZE
	 * @throws CipherEngineException
	 */
	public static void generateKeys() throws CipherEngineException {
		generateKeys(DEFAULT_SIZE);
	}
	
	/**
	 * Generate keys yang digunakan untuk proses enkripsi supaya siap pakai
	 * @param num Jumlah key yang digenerate
	 * @throws CipherEngineException
	 */
	public static void generateKeys(int num) throws CipherEngineException {
		int size = num;
		if(size > MAX_SIZE) {
			size = MAX_SIZE;
		}
		for(int i=0; i<size; i++) {
			String idxKey = String.valueOf(i);
			InMemoryRSAKey key = new InMemoryRSAKey(idxKey);
			generatedKeys.put(idxKey, key);
		}
	}
	
	/**
	 * Meminta satu key dari memory secara random
	 * @return InMemoryRSAKey siap pakai
	 */
	public static InMemoryRSAKey getKey() throws CipherEngineException {
		if(generatedKeys.isEmpty()) {
			throw new CipherEngineException("Kunci enkripsi belum digenerate ketika aplikasi started");
		}
		String idx = String.valueOf(new Random().nextInt(generatedKeys.size()));
		return generatedKeys.get(idx);
	}
}
