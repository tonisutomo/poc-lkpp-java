package models.jcommon.cdn.client;


import models.jcommon.db.base.BaseModel;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

@Table(name="CDN_HOST")
public class CDNHost extends BaseModel {
	@Id
	public Long cdn_host_id;
	public String name;
	public String url;
	public boolean active;
	
}
