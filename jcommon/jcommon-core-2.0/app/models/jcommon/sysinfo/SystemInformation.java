package models.jcommon.sysinfo;

import java.io.File;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import ext.FormatUtils;
import models.jcommon.blob.BlobTableDao;
import play.Play;
import play.cache.Cache;
import play.db.DB;
import play.db.jdbc.Query;

/**Get system information
 * 
 * @author Mr. Andik
 *
 */
public class SystemInformation implements Serializable{
	
	private static Date systemStart=new Date();
	private File file=null;
	
	public Map<String, Object> informations;
	
	private SystemInformation()
	{
		informations=new HashMap<String, Object>();
		DiskSpace freeFileStorage=getFileStorageSpace();
		informations.put("Free File Storage", freeFileStorage);
		informations.put("Free Temp Storage", getPlayTemporarySpace());
		informations.put("System Start", FormatUtils.formatDateTimeInd(systemStart));
		informations.put("Java Free Memory", FormatUtils.formatBytes(Runtime.getRuntime().freeMemory()));
		informations.put("Java Total Memory*", FormatUtils.formatBytes(Runtime.getRuntime().totalMemory()));
		informations.put("Java Max Memory*", FormatUtils.formatBytes(Runtime.getRuntime().maxMemory()));
		informations.put("Core", Runtime.getRuntime().availableProcessors());
		informations.put("Database Tablespace Size (pg_default)", FormatUtils.formatBytes(getDatabaseSize()));
		informations.put("JDK", String.format("%s %sbit - %s ", 
				System.getProperty("java.runtime.version"),
				System.getProperty("sun.arch.data.model"),
				System.getProperty("os.name")));
	}
	
	public static SystemInformation getSystemInformation()
	{
		SystemInformation sys=Cache.get("models.jcommon.sysinfo.SystemInformation", SystemInformation.class);
		if(sys==null)
		{
			sys=new SystemInformation();
			Cache.set("models.jcommon.sysinfo.SystemInformation",  sys, "1mn");
		}
		return sys;
	}
	
	private enum DIRECTORY
	{
		TEMP,
		STORAGE_DIR
	}
	
	private File getDirectory(DIRECTORY dir)
	{
		if(dir==DIRECTORY.TEMP)
			file=Play.tmpDir;
		else
			file=new File(BlobTableDao.getFileStorageDir());
		return file;
	}
	
	public Date getSystemStartTime()
	{
		return systemStart;
	}	
	
	
	private DiskSpace getDiskSpace(DIRECTORY dir)
	{
		File file=getDirectory(dir);
        return new DiskSpace(file);
	}
	
	
	public DiskSpace getFileStorageSpace()
	{
		return getDiskSpace(DIRECTORY.STORAGE_DIR);
	}
	
	public DiskSpace getPlayTemporarySpace()
	{
		return getDiskSpace(DIRECTORY.TEMP);
	}
	
	public long getDatabaseSize()
	{
		//khusus postgres
		if(Play.configuration.getProperty("db.default.driver").contains("postgres"))
			return Query.find("SELECT pg_tablespace_size('pg_default')", Long.class).first();
		else
			return 0;
	}

}
