package models.jcommon.http;

import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.zip.GZIPInputStream;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.jboss.netty.handler.codec.http.HttpHeaders;

import play.Logger;
import play.Play;
import play.libs.WS;
import play.libs.WS.HttpResponse;
import play.libs.WS.WSRequest;
import play.mvc.Http.Header;



/**
 * RawHttpRequest Class ini digunakan sebagai pengganti WS dari play. Apa
 * kelebihannya? Class ini support gzip encoding. Dengan cara ini, request
 * header ditambahkan 'accept-encoding: gzip, deflate' yang menyebabkan
 * WebServer mengirim response dalam format gzip. Secara umum, ukuran response
 * menjadi 90% dari data aslinya
 * 
 * Setting proxy diambil dari conf/application.conf
 * 
 * Cara pengunaan:
 * 
 * SimpleHttpRequest req=new SimpleHttpRequest();
 * req.get(url);
 * Catatan:
 * Hanya mendukung GET method
 * 
 * 
 * @author Mr. Andik
 *
 */
public class SimpleHttpRequest implements Closeable{

	private InputStream is;
	private HttpResponse conn;
	private String encoding;
	private int timeout;
	private Long length;
	static {
		//FIXING ERROR: ERROR javax.net.ssl.SSLHandshakeException: java.security.cert.CertificateException: No subject alternative DNS name matching www.lpse.depkeu.go.id found
		/*HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
			public boolean verify(String hostname, SSLSession session) {
				// ip address of the service URL(like.23.28.244.244)
				return true;

			}
		});
		TrustAllCertificates.install();*/
		
		String proxyHost = Play.configuration.getProperty("http.proxyHost");
		String proxyPort = Play.configuration.getProperty("http.proxyPort");
		String proxyHostHttps = Play.configuration.getProperty("https.proxyHost", proxyHost);
		String proxyPortHttps = Play.configuration.getProperty("https.proxyPort", proxyPort);
		String proxyUser = Play.configuration.getProperty("http.proxyUser");
		String proxyPassword = Play.configuration
				.getProperty("http.proxyPassword");
		if (!StringUtils.isEmpty(proxyHost))
			System.setProperty("http.proxyHost", proxyHost);
		if (!StringUtils.isEmpty(proxyPort))
			System.setProperty("http.proxyPort", proxyPort);
		if (!StringUtils.isEmpty(proxyHostHttps))
			System.setProperty("https.proxyHost", proxyHostHttps);
		if (!StringUtils.isEmpty(proxyPortHttps))
			System.setProperty("https.proxyPort", proxyPortHttps);

		if (!StringUtils.isEmpty(proxyUser))
			System.setProperty("http.proxyUser", proxyUser);
		if (!StringUtils.isEmpty(proxyPassword))
			System.setProperty("http.proxyPassword", proxyPassword);
	}

	public SimpleHttpRequest() {
	}
	
	public SimpleHttpRequest(int secondsTimeout) {
		this.timeout=secondsTimeout*1000;
	}


	/**
	 * Membuka koneksi ke URL tujuan
	 * 
	 * @param url
	 * @throws IOException
	 */
	public void get(String url) throws Exception {
		get(url, true);
	}
	
	/**Membuka koneksi ke URL tujuan
	 * 
	 * Response akan disimpan ke file temporary sebelum return to caller.
	 * Untuk request dengan URL yang sama, akan dicek berdasarkan Etag apakah
	 * perlu request lagi ke server atau cukup ambil dari temporary.
	 * 
	 * TempFileManager tidak digunakan karena kita perlu temporary yang lebih permanent.
	 * Kalau di TempFileManager akan di-delete pada durasi tertentu
	 * 
	 * @param url
	 * @throws IOException
	 */
	public void get(String url, boolean gzip) throws Exception {
		
//		File fileEtag=new File(FileUtils.getTempDirectory()+"/jcommon/" + DigestUtils.md5Hex(url) + ".etag");
//		File file=new File(FileUtils.getTempDirectory()+"/jcommon/" + DigestUtils.md5Hex(url));
		try {		
			WSRequest request = WS.url(url);
			if(gzip) 
				request.setHeader("accept-encoding", "gzip, deflate");

			//dapatkan informasi etag dan Date dari request sejenis (dengan url sama) 			
//			file.getParentFile().mkdirs();			
//			if(fileEtag.exists())
//			{
//				InputStream isEtag=new FileInputStream(fileEtag);
//				String eTagData=IOUtils.toString(isEtag);
//				isEtag.close();
//				String[] ary=eTagData.split("\n");
//				request.setHeader("If-Modified-Since", ary[0]);
//				request.setHeader("If-None-Match", ary[1]);
//			}
			
			if(timeout > 0)
				request.timeout = timeout;
			conn = request.getAsync().get();
			is = conn.getStream();
			
			String location=conn.getHeader("Location");
			if(location!=null)
				if(!url.equals(location))
					get(location);		
			
		}catch (Exception e) {
			Logger.error(e,"kendala akses URL : %s", url);
		} 		
		/*Dapatkan header e-tag dan Date untuk digunakan pada request berikutnya.
		 * Simpan hasil request ini ke file
		 * 
		 */
//		if(conn.getStatus()== 200) // ACCEPTED
//		{
//			OutputStream out=new FileOutputStream(file);
//			IOUtils.copy(is, out);
//			out.close();
			
//			String date=conn.getHeader("Date");
//			String etag=conn.getHeader("Etag");
//			OutputStream outEtag=new FileOutputStream(fileEtag);
//			IOUtils.write(date + "\n" + etag, outEtag);
//			outEtag.close();

//			Logger.debug("URL: %s, new, save to: %s", url, file);
			
//		}
//		is=new FileInputStream(file);

		if(gzip) {
			String encoding = conn.getHeader("Content-Encoding");
			if ("gzip".equalsIgnoreCase(encoding))
				is = new GZIPInputStream(is);
		}
		try {
			length = Long.parseLong(conn.getHeader(HttpHeaders.Names.CONTENT_LENGTH));
		}catch (NumberFormatException e) {
			length = -1L;
		}
	}

	/**
	 * Mendapatkan inputstream dari response. Jika ingin diolah sebagai
	 * inputStream
	 * 
	 * @return
	 */
	public InputStream getInputStream() {
		return is;
	}

	/**
	 * Mendapatkan String dari response. Jika ingin diolah sebagai string
	 * 
	 * @return
	 */
	public String getString() throws IOException {
		encoding = conn.getHeader("Content-Encoding");
		if(length==-1)
			length=1024L;
		int buffLength = 1024;
		if ("gzip".equalsIgnoreCase(encoding))
			buffLength = length.intValue() * 100;
		else if (length > 0) {
			buffLength = length.intValue();
		}
		if (is == null)
			return null;
		ByteArrayOutputStream out = new ByteArrayOutputStream(buffLength);
		IOUtils.copy(is, out);
		out.close();
		return out.toString();
	}

	public String getEncoding() {
		return encoding;
	}

	public Long getLength() {
		return length;
	}

	public String getStatusText() {
		return conn.getStatusText();
	}
	
	public int getStatus() {
		return conn.getStatus();
	}
	
	public List<Header> getHeaders()
	{
		return conn.getHeaders();
	}

	@Override
	public void close() throws IOException {
		if(is != null)
			is.close();		
	}

}
