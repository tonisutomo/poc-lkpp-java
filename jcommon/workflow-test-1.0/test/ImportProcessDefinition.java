import java.io.FileInputStream;

import models.workflow.definition.Workflow;

import org.junit.Test;

import play.cache.Cache;
import play.test.FunctionalTest;

public class ImportProcessDefinition extends FunctionalTest {

    @Test
    public void testImportProcessDefinition() throws Exception {
    	Cache.add("jcommon.workflow.browsing.enable", true);
       // WorkflowDao.importProcessDefinition(new FileInputStream("D:/Bappenas/eproc4/jcommon/trunk/workflow/test/data/Pasca_1S_GUGUR.csv"), true);
    	//WorkflowDao.importProcessDefinition(new FileInputStream("test/data/PRA_2S_KUA_Bagian1.csv"), true);
    	Workflow.importProcessDefinition(new FileInputStream("test/data/PASCA_1S_GUGUR_N2.csv"), true);
    }
    
}