package play.modules.undertow;

import java.io.File;
import java.lang.management.ManagementFactory;
import java.util.Properties;

import org.xnio.Options;

import io.undertow.Undertow;
import play.Logger;
import play.Play;
import play.libs.IO;

public class Server {

	public final static String PID_FILE = "server.pid";	

	public Server(String[] args) {
		System.setProperty("file.encoding", "utf-8");
		Properties p = Play.configuration;

		int httpPort = Integer.parseInt(getOpt(args, "http.port", p.getProperty("http.port", "-1")));
		int httpsPort = Integer.parseInt(getOpt(args, "https.port", p.getProperty("https.port", "-1")));

		if (httpPort == -1 && httpsPort == -1) {
			httpPort = 9000;
		}

		if (httpPort == httpsPort) {
			Logger.error("Could not bind on https and http on the same port " + httpPort);
			Play.fatalServerErrorOccurred();
		}
		String address = "0.0.0.0";
		String secureAddress = "0.0.0.0";
		if (p.getProperty("http.address") != null) {
			address = p.getProperty("http.address");
		} else if (System.getProperties().containsKey("http.address")) {
			address = System.getProperty("http.address");
		}
		if (p.getProperty("https.address") != null) {
			secureAddress = p.getProperty("https.address");
		} else if (System.getProperties().containsKey("https.address")) {
			secureAddress = System.getProperty("https.address");
		}
		try {			
			if (httpPort != -1) {			
				 Undertow server = Undertow.builder().addHttpListener(httpPort, address).setSocketOption(Options.BACKLOG, 100000).setHandler(new PlayHandler()).build();
				 server.start();
			}
			if (httpsPort != -1) {
				 Undertow server = Undertow.builder().addHttpListener(httpsPort, secureAddress).setSocketOption(Options.BACKLOG, 100000).setHandler(new PlayHandler()).build();
				 server.start();
			}
		} catch (Exception e) {
			e.printStackTrace();
			Logger.error("Could not bind on port " + httpPort, e);
			Play.fatalServerErrorOccurred();
		}
	}
	
	private static void writePID(File root) {
		String pid = ManagementFactory.getRuntimeMXBean().getName().split("@")[0];
		File pidfile = new File(root, PID_FILE);
		if (pidfile.exists()) {
			throw new RuntimeException("The " + PID_FILE + " already exists. Is the server already running?");
		}
		IO.write(pid.getBytes(), pidfile);
	}

	private String getOpt(String[] args, String arg, String defaultValue) {
		String s = "--" + arg + "=";
		for (String a : args) {
			if (a.startsWith(s)) {
				return a.substring(s.length());
			}
		}
		return defaultValue;
	}

	public static void main(String[] args) throws Exception {
		File root = new File(System.getProperty("application.path"));
		if (System.getProperty("precompiled", "false").equals("true")) {
			Play.usePrecompiled = true;
		}
		if (System.getProperty("writepid", "false").equals("true")) {
			writePID(root);
		}
		Play.init(root, System.getProperty("play.id", ""));
		if (System.getProperty("precompile") == null) {
			new Server(args);
		} else {
			Logger.info("Done.");
		}
	}
}
