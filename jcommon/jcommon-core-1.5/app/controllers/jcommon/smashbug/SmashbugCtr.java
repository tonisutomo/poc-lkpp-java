/**
 * 
 */
package controllers.jcommon.smashbug;

import java.io.ByteArrayInputStream;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;

import org.apache.commons.codec.binary.Base64;

import models.jcommon.secure.encrypt.CipherEngineException;
import models.jcommon.secure.encrypt2.CachedKeyCipherEngine;
import models.jcommon.secure.encrypt2.CachedRSAKey;
import models.jcommon.secure.encrypt2.CipherEngine2;
import models.jcommon.secure.encrypt2.EncryptCipherEngine;
import models.jcommon.smashbug.LogFile;
import play.Logger;
import play.cache.Cache;
import play.mvc.Controller;

/**
 * @author AndikYulianto@yahoo.com
 *
 */
public class SmashbugCtr extends Controller {

	/**Mencari detil dari error di logs
	 * @param sessionId
	 * @throws CipherEngineException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 * 
	 * Contoh penggunaan bisa dilihat di log.LogFileErrorMessageTest (project spse-4)
	 * 
	 *Step 1
	 * Server mengirim sessionId serta publicKey dari milik server -> PubServer
	 * SPSE merespon dengan mengirim publicKey dari SPSE (pubSpse) yg di-encrypt dengan pubServer
	 * 
	 *Step 2
	 * Request:
	 * a. Server mendapatkan pubSpse dan digunakan untuk melakukan enkripsi terdapat bugId -> encryptedBugId
	 * b. Server mengirimkan encryptedBugId serta PubServer
	 * SPSE Response:
	 * a. SPSE melakukan dekripsi terhadap bugId
	 * b. SPSE mencari bug dari semua file di folder logs -> log_content
	 *    Dilakuan terhadap file terbaru hingga file terlama sampai ditemukan
	 * c. SPSE melakukan enkripsi log_content menggunakan PubServer
	 * d. Server melakukan dekripsi log_content
	 * 
	 * Uji coba
	 * Dengan jumlah 50 file log (total 641 MB) memerlukan waktu 1,3 detik.
	 */
	public static void getLogFileErrorMessageStep1(String sessionId) throws IllegalBlockSizeException, BadPaddingException, CipherEngineException
	{
		String publikKey=request.params.get("publicKey");
		CachedRSAKey key=new CachedRSAKey();
		CipherEngine2 ce= new EncryptCipherEngine(publikKey); //gunakan publikKey dari Server untuk melakukan enkripsi
		byte[] result=ce.doCrypto(key.getPublicKeyEncoded().getBytes());
		response.setHeader("X-session", key.sessionKey);//tambahkan ini untuk session id
		Logger.debug("[SPSE#1]  spseSessionId: %s -> spsePubkey: %s", key.sessionKey, key.getPublicKeyEncoded());
		renderBinary(new ByteArrayInputStream(result));
	}
	
	public static void getLogFileErrorMessageStep2(String spseSessionId) throws CipherEngineException
	{
		String bugId=params.get("bugId");
		String publicKeyServer=params.get("publicKey");
		byte[] ary=Base64.decodeBase64(bugId);
		CachedKeyCipherEngine ce=new CachedKeyCipherEngine(Cipher.DECRYPT_MODE, spseSessionId, false);
		bugId=new String(ce.doCrypto(ary));

		Logger.debug("[SPSE]  spseSessionId: %s -> bug_id: %s", spseSessionId, bugId);

		String key="controllers.admin.UtilityCtr.internalServerErrorStep2(String)" + bugId;
		String logMessage=Cache.get(key,String.class);
		if(logMessage==null)
		{
			logMessage=new LogFile().findLogMessage(bugId);
			if(logMessage==null)
				logMessage="NotFound";
			Cache.set(key, logMessage, "24h");//simpan di cache selama 24 jam
		}
		
		CipherEngine2 ecryptor= new EncryptCipherEngine(publicKeyServer); //gunakan publikKey dari Server untuk melakukan enkripsi
		byte[] result=ecryptor.doCrypto(logMessage.getBytes());
		
		renderBinary(new ByteArrayInputStream(result));
	}
	
	
}
