package controllers.jcommon.http;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.RandomAccessFile;
import java.util.Arrays;
import javax.servlet.http.HttpServletResponse;
import play.Logger;
import play.libs.MimeTypes;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Http.Header;


/**Class ini menyediakan method untuk untuk download file yang bisa di-resume
 * Cara penggunaan controllers.jcommon.http.ResumableDownloadController.renderBinaryResumable(File, String)
 * 
 * ResumableDownload tidak diperlukan lagi karena pada play-1.2.7a-lkpp sudah support resumable 
 * asalkan menggunakan renderBinary(File);
 *  */

@Deprecated
public class ResumableDownloadController extends Controller{
	
	/** Render binary ini resumable, accept range
	 * @param file
	 * @param fileName fileName in response header
	 * @throws FileNotFoundException 
	 */
	
	private final static  int BUF_SIZE=1024;
	private final static  int READ_FILE_BUFFER=1024;//10Kb, pernah dicoba angka 50kb tapi out of memory


	protected static void renderBinaryResumable(File file, String fileName) throws FileNotFoundException 
	{
		
		if(!file.exists())
		{
			// response.sendError(HttpServletResponse.SC_NOT_FOUND);
			Logger.error("File not found: " + file.getAbsolutePath());
			notFound("File not found: " + fileName);
//			throw new FileNotFoundException("File not found." + file.toString());							
		}
	
		
		String requestRange = getRequestHeader("Range");
		if(requestRange==null)
			requestRange = getRequestHeader("range");
		long fileSize = file.length();
		setHeaderHttpResponse(fileName, (int)fileSize);
		response.setHeader("Accept-Ranges", "bytes");		
		
		response.contentType = MimeTypes.getContentType(fileName);		
		int reqStart=0;
		int reqEnd=(int)fileSize-1;
		int	contentLength=(int)fileSize;
		if (requestRange != null) {
			Logger.debug("[Request header] Range: %s",  requestRange);
			String[] aryRange = getHeaderRangeValue(requestRange, fileSize);
			reqStart=Integer.parseInt(aryRange[0]);
			reqEnd=Integer.parseInt(aryRange[1]);
			if(reqStart>reqEnd)
			{
				Logger.debug("Request Range Error (range may exceed fileSize: %,d",fileSize);
				response.status=HttpServletResponse.SC_REQUESTED_RANGE_NOT_SATISFIABLE;
				return ;
			};
			String str="bytes " + reqStart + "-" + reqEnd + "/" + fileSize;
			response.setHeader("Content-Range",str);
			
			Logger.debug("Accept-ranges: %s", str);
			
			contentLength=(reqEnd-reqStart)+1;	
		}
		else {
			contentLength=(int)fileSize;
		}
		response.status=HttpServletResponse.SC_PARTIAL_CONTENT;//untuk chunked
	//	response.setHeader("Content-Length", String.valueOf(contentLength));
		Logger.debug("Content-Length: %,d", contentLength);
		Logger.debug("Download file: %s, Start: %,d End: %,d ", file.toString(), reqStart,reqEnd);
				
		fileDownloadChunked(file, reqStart, reqEnd);
		
	}

	
	private static void fileDownloadChunked(File file, int start, int end) throws FileNotFoundException{				
		byte buffer[]=new byte[READ_FILE_BUFFER];
		boolean endOfFile=false;
		
		while(!endOfFile)
		{
			int numberOfByteRead=0;
			int numberOfByteReadPrev=0;
			//repeat until 'end' reach
			while(true)
			{
				//Keep exclusive during reading file
				synchronized (request.current()) {
					//Improved version using RandomAccessFile
					RandomAccessFile ras=null;					
					try
					{	ras=new RandomAccessFile(file, "r");
						if(start!=0)
							ras.seek(start);
						numberOfByteRead=ras.read(buffer);
						ras.close();//close file immediately */
					}
					catch(Exception e)
					{
						throw new FileNotFoundException(e.toString());
					}
					finally
					{
						try
						{
							if(ras!=null)
								ras.close();
						}
						catch(Exception e){};
					}
				}
				
				if(numberOfByteRead==-1)
				{
					Logger.trace("File %s, block read completed: last #bytes %s", file.getName(), numberOfByteReadPrev);
					endOfFile=true;
					break;
				}
				numberOfByteReadPrev=numberOfByteRead;				
				start+=numberOfByteRead;//read next block
				try
				{
					byte[] byteToWrite;
					if(numberOfByteRead==BUF_SIZE)
						byteToWrite=buffer;
					else
						byteToWrite=Arrays.copyOf(buffer, numberOfByteRead);
				
					try 
					{ 							
						response.writeChunk(byteToWrite);
					}
					catch(OutOfMemoryError e)
					{
						Logger.error("[ResumableDownloadController] Error while writing to buffer. %s");
						e.printStackTrace();
						response.status=Http.StatusCode.INTERNAL_ERROR;
						return;
					}
				}
				catch(Exception e)
				{//error mungkin terjadi jika client di-close 
					if(!e.getCause().getMessage().contains("closed"))
						e.printStackTrace();
						
				}
			}
		}
		
	}
	

	private static void fileDownload(File file, int start, int end) throws FileNotFoundException{				
		byte buffer[]=new byte[READ_FILE_BUFFER];
		boolean endOfFile=false;
		
		while(!endOfFile)
		{
			int numberOfByteRead=0;
			int numberOfByteReadPrev=0;
			//repeat until 'end' reach
			while(true)
			{
				//Keep exclusive during reading file
				synchronized (request.current()) {
					//Improved version using RandomAccessFile
					RandomAccessFile ras=null;					
					try
					{	ras=new RandomAccessFile(file, "r");
						if(start!=0)
							ras.seek(start);
						numberOfByteRead=ras.read(buffer);
						ras.close();//close file immediately */
					}
					catch(Exception e)
					{
						throw new FileNotFoundException(e.toString());
					}
					finally
					{
						try
						{
							if(ras!=null)
								ras.close();
						}
						catch(Exception e){};
					}
				}
				
				if(numberOfByteRead==-1)
				{
					Logger.trace("File %s, block read completed: last #bytes %s", file.getName(), numberOfByteReadPrev);
					endOfFile=true;
					break;
				}
				numberOfByteReadPrev=numberOfByteRead;				
				start+=numberOfByteRead;//read next block
				try
				{
					byte[] byteToWrite;
					if(numberOfByteRead==BUF_SIZE)
						byteToWrite=buffer;
					else
						byteToWrite=Arrays.copyOf(buffer, numberOfByteRead);
				
					try 
					{ 							
						response.out.write(byteToWrite);
					}
					catch(OutOfMemoryError e)
					{
						Logger.error("[ResumableDownloadController] Error while writing to buffer. %s");
						e.printStackTrace();
						response.status=Http.StatusCode.INTERNAL_ERROR;
						return;
					}
				}
				catch(Exception e)
				{//error mungkin terjadi jika client di-close 
					if(!e.getCause().getMessage().contains("closed"))
						e.printStackTrace();
						
				}
			}
		}
		
	}
	
	
	private static String[] getHeaderRangeValue(String requestRange, long fileSize){
		requestRange = requestRange.toLowerCase();		
		requestRange = requestRange.replaceAll("bytes=", "");
		if(requestRange.startsWith("-"))
			requestRange="0"+requestRange;
		if(requestRange.endsWith("-"))
			requestRange=requestRange+(fileSize-1);		
		return requestRange.split("-");		
	}
	
	private static void setHeaderHttpResponse(String filename, int filesize){
		String contentType = MimeTypes.getContentType(filename);
		response.contentType=contentType;

		response.setHeader("Content-Disposition", "attachment; filename=\""+filename+"\"");
		response.setHeader("Cache-control", "no-cache");
		response.setHeader("Cache-control", "no-store");
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Expires", "0");
	}	
	

	private static String getRequestHeader(String key) {
		Header header=request.headers.get(key);
		if(header==null)
			return null;
		else
			return header.value();
	}
}
