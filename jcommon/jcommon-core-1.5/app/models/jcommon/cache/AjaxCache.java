package models.jcommon.cache;

import java.io.Serializable;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.codec.digest.DigestUtils;
import play.cache.Cache;
import play.mvc.Http;
import play.mvc.Http.Header;
import play.mvc.Http.Request;
import play.mvc.Http.Response;

/**
 * Untuk management Cache dari Controller.
 * Cara kerja Pada Action yang akan di-cache: 
 * 1. Siapkan implementasi dari AjaxCacheProcessor
 *    Isinya, adalah String yang akan di-render pada Action tersebut
 * 2. AjaxCache.getInstance().execute(processor ,request, response, parentCache);
 * 		-> processor: instance dari class point #1
 * 		-> parentCache yang akan dijadikan Key pada Play Cache. Bisa berupa nama class dari Model yang di-query
 * 		   Dapat juga dikombinasikan dengan ID tertentu. Contoh pada Paket_Rup dari satker_id xxx; parentKey 
 * 			dapat diisi: Paket_Rup:xxx
 * 
 * Key yang digunakan adalah Class tersebut. Jika Cache harus di-invalidate (misal karena ada data baru yg diinsert ke DB)
 * maka panggil AjaxCache.getInstance().invalidate(Class);
 * 
 * @author Mr. Andik
 *
 */
public class AjaxCache implements Serializable {

	private static String KEY = "models.cache.AjaxCache";
	private static String CACHE_EXPIRES="12h";

	private static class CacheItem
	{
		public String md5;
		public CacheItem( String value) {
			super();
			this.md5 = DigestUtils.md5Hex(value);
			this.value = value;
		}
		public String value;
	}
	
	//prevent creation from other class
	private AjaxCache() {
	};

	//single instance
	public synchronized static AjaxCache getInstance() {
		AjaxCache cache = Cache.get(KEY, AjaxCache.class);
		if (cache == null) {
			cache = new AjaxCache();
			Cache.set(KEY, cache, CACHE_EXPIRES);// set for 12H
		}

		return cache;
	}

	/**
	 * Reset cache for clazz
	 * 
	 * @param parentCache
	 */
	public void invalidate(String parentCache) {
		Cache.delete(parentCache);
	}

	/**
	 * put and return CacheItem
	 * @param parentCache
	 * @param url
	 * @param cacheValue
	 * @return
	 */
	private synchronized CacheItem put(String parentCache, String url, String cacheValue) {
		Map<String, CacheItem> cacheMap=Cache.get(parentCache, Map.class);
		CacheItem ci=null;
		if(cacheMap==null)
		{
			cacheMap=new ConcurrentHashMap<>();
			Cache.set(parentCache, cacheMap, CACHE_EXPIRES);
			ci=new CacheItem(cacheValue);
			cacheMap.put(url, ci);
			return ci;
		}
		else
		{
			ci=cacheMap.get(url);
			if(ci==null)
			{
				ci=new CacheItem(cacheValue);
				cacheMap.put(url, ci);
			}
		}
		return ci;
	}
	
	private synchronized CacheItem get(String parentCache, String url)
	{
		Map<String, CacheItem> cacheMap=Cache.get(parentCache, Map.class);
		CacheItem ci=null;
		if(cacheMap!=null)
			ci=cacheMap.get(url);
		return ci;
	}
	

	public synchronized void execute(AjaxCacheProcessor ajaxCacheProcessor,
		Request request, Response response, String parentCache) {
		// check etag of request
		Header headerETag = request.headers.get("if-none-match");
		if (headerETag == null)
		{
			//#1 Lihat di Cache, ada datanya apa tidak?
			String url=request.url;
			CacheItem ci=get(parentCache, url);
			if(ci==null)
			{
			//#Jika tidak ada di cache, maka ambil dari DB	
				String data=ajaxCacheProcessor.process();
				ci=put(parentCache, url, data);
			}
			
			response.contentType="text/json";
			response.headers.put("ETag", new Header("ETag", ci.md5));
			response.writeChunk(ci.value);
		}
		else
			// not modified, don't send data
			response.status = Http.StatusCode.NOT_MODIFIED;
	}

}
