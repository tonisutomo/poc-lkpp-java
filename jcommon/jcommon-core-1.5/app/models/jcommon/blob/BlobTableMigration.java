package models.jcommon.blob;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.security.NoSuchAlgorithmException;

import javax.persistence.Id;

import play.Logger;
import play.Play;
import play.Play.Mode;

/**Model ini digunakan hanya untuk development migrasi.
 * Ini merupakan representasi BlobTable versi 3.5
 * 
 * @author andik
 *
 */
public class BlobTableMigration extends BlobTable {
	
	@Id
	public Long blb_id_content;

	@Id
	public Integer blb_versi;	
	public String blb_nama_file;
	public Long blb_ukuran;
	public String blb_hash;
	
	public String blb_path;
	
	public File getFile()
	{
		if(blb_nama_file==null)
			throw new NullPointerException("blb_nama_file is null for id" + blb_id_content);
		return new File(BlobTableDao.getFileStorageDir() + "/" + blb_path  + "/" + blb_nama_file);
	}
	
	/**
	 * Lakukan migrasi dari SPSE 3 ke SPSE 4
	 * Statistik: 5GB data perlu 7 menit (pada PC)
	 * 
	 * @return true jika berhasil migrasi, false jika tidak ada lagi yang dimigrasi 
	 * @throws IOException 
	 * @throws NoSuchAlgorithmException 
	 */
	public static boolean migrateSingleFile(BlobTable blob, PrintStream out) throws NoSuchAlgorithmException, IOException
	{
		blob.blb_engine=BlobTable.ENGINE_VERSION;
		long size=0;
		if(blob.getFile().exists())
			size=blob.getFile().length();
		out.format("Source file [%s,%s] hash: %s file: %s size: %,d \n", blob.blb_id_content, blob.blb_versi, blob.blb_hash, blob.getFile(), size);
		if(!blob.getFile().exists())
		{
			Logger.error("Migration failed, source file not found: %s", blob.getFile());
			out.format("\tNot Found\n");
			return false;
		}
		else
		{
			//result[0]: path
			//result[1]: hash
			//result[2]: size
			String[] result=BlobTableDao.saveFileReturnArray(ARCHIEVE_MODE.ARCHIEVE, blob.getFile());	
			out.format("\tResult: hash: %s file: %s size: %,d\n", result[1], result[0], Long.parseLong(result[2]));
		
			//delete source file if in production
			if(Play.mode==Mode.PROD)
				blob.getFile().delete();
			
			blob.blb_path=result[0];
			blob.blb_hash=result[1];
			Logger.debug("Converted from %s to %s", blob.getFile(), result[0]);
			blob.blb_engine="4.0.0";
			blob.saveAndCommit();
			return true;
		}
	}
	
}
