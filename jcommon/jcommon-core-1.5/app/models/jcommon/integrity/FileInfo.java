package models.jcommon.integrity;


public class FileInfo
{
	public FileInfo(long size, String hash, String name) {
		super();
		this.size = size;
		this.hash = hash;
		this.name = name.replace('\\', '/'); //convert as '/'
	}
	public long size;
	public String hash;
	public String name;
	
	public int hashCode()
	{
		return name.hashCode();
	}
	
	/**
	 * equals if name & hash equal
	 */
	public boolean equals(Object o)
	{
		FileInfo i=(FileInfo)o;
		boolean eq=i.name.equals(name) && i.hash.equals(hash);
		return eq;
	}
	
	public String toString()
	{
		return String.format("%s, hash: %s, size: %,d", name, hash, size);
	}
	
}