package models.jcommon.secure.encrypt2;

import javax.crypto.Cipher;

import models.jcommon.secure.encrypt.CipherEngineException;

public class EncryptCipherEngine extends CipherEngine2 {

	public EncryptCipherEngine(String cipherKey) throws CipherEngineException {
		super(Cipher.ENCRYPT_MODE, cipherKey);
	}

}
