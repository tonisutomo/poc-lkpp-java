package models.jcommon.db.query;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.concurrent.CountDownLatch;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.apache.commons.lang.time.StopWatch;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.postgresql.copy.CopyManager;
import org.postgresql.jdbc.PgConnection;
import org.sql2o.Connection;
import org.sql2o.Sql2o;
import org.sql2o.quirks.QuirksDetector;

import models.jcommon.util.TempFileManager;
import play.Logger;
import play.Play;
import play.jobs.Job;

public abstract class BaseTableCopier  {
	
	public class DBConnection
	{
		public String dbUrl;
		public String dbPass;
		public String dbUser;
		public String dbDriver;
		
		public String toString()
		{
			return dbUrl;
		}
	}
	
	private CountDownLatch latch;
	
	private String sql;
	protected Document doc;
	protected DBConnection dbSource;
	protected DBConnection dbTarget;

	private int sqlCount=0;
	long totalRows=0;
	
	protected BaseTableCopier() throws IOException {
	
		String name= this.getClass().getName();
		name=name.replace('.', '/')+ ".html";
		File file=Play.getFile("app/"+name);
		doc= Jsoup.parse(file, "UTF-8");
		
		//dapatkan info Koneksi source		
		dbSource=new DBConnection();
		dbSource.dbDriver=getValue("div#dbSource > span[name=db.driver]");
		dbSource.dbUrl=getValue("div#dbSource > span[name=db.url]");
		dbSource.dbUser=getValue("div#dbSource > span[name=db.user]");
		dbSource.dbPass=getValue("div#dbSource > span[name=db.pass]");
		
		dbTarget=new DBConnection();
		dbTarget.dbDriver=getValue("div#dbTarget > span[name=db.driver]");
		dbTarget.dbUrl=getValue("div#dbTarget > span[name=db.url]");
		dbTarget.dbUser=getValue("div#dbTarget > span[name=db.user]");
		dbTarget.dbPass=getValue("div#dbTarget > span[name=db.pass]");
		
		//simpan ini harus di Job baru 
		new Job()
		{
			public void doJob()
			{
				
			}
		}.now();
		
		
	}

	private String getValue(String cssSelector) {
		Elements elements= doc.select(cssSelector);
		if(elements.size()==1)
			return elements.first().text();
		else
			return null;
	}

	
	public void execute() {
		
		Logger.debug("[START] executing dbSource: %s, dbTarget: %s", dbSource, dbTarget);
		
		Elements elements=doc.select("div#copy-definitions > div");
		latch=new CountDownLatch(elements.size());
		String targetTable="";
		try {
			StopWatch swAll=new StopWatch();
			swAll.start();
			
			PgConnection connSource= (PgConnection)beginTransaction(dbSource).getJdbcConnection();
			
			int index=1;
			for(Element el: elements)
			{
				sql=el.text();
				targetTable=el.id();
		
				sql=String.format("COPY (%s) TO stdout CSV HEADER", sql);			
				StopWatch sw=new  StopWatch();
				sw.start();
				CopyManager cm=new CopyManager(connSource);
				
				//1. Copy from dbSource to temporary file
				Logger.debug("\t[START %s/%s] %s", index, elements.size(), targetTable);
				File file=TempFileManager.createFileInTemporaryFolder(targetTable);
				FileOutputStream out=new FileOutputStream(file);
				cm.copyOut(sql, new GZIPOutputStream(out));			
				out.close();				
				sw.stop();
				Logger.debug("\t[DONE %s/%s] duration %s, file: %s, size: %d", index++, elements.size(), sw, file, file.length());
				
				copyInto(file);
				
			}
			swAll.suspend();
			Logger.info("\t[DONE Copy from Server] duration: %s, now waiting for insert into target....", swAll);
			swAll.resume();
			latch.await();
			swAll.stop();
			Logger.info("[DONE TableCopier] #SQL: %s, duration: %s, total rows: %,d", sqlCount, swAll, totalRows);
			
		} catch ( SQLException | IOException | InterruptedException e) {
			Logger.error(e, "Error %s while executing SQL.\nTarget: %s, SQL: %s", targetTable, sql);
		}
		
	}

	//copy file into target database as separated thread
	private void copyInto(final File file) {
		final String tableName=file.getName();

		new Thread(tableName)
		{
			@Override
			public void run()
			{
				PgConnection connTarget= (PgConnection)beginTransaction(dbTarget).getJdbcConnection();
				String sql="";
			
				Logger.debug("\t[CopyInto thread#%s] running for file: %s", tableName, file);
				StopWatch sw=new StopWatch();
				sw.start();
				
				try
				{
					sql=String.format("TRUNCATE TABLE %s CASCADE", tableName);
					connTarget.execSQLUpdate(sql);
					CopyManager cm=new CopyManager(connTarget);						
					sql=String.format("COPY %s FROM STDIN CSV HEADER", tableName);
					InputStream is=new GZIPInputStream(new FileInputStream(file));
					long rows=cm.copyIn(sql, is);
					totalRows+=rows;
					connTarget.commit();
					sw.stop();
					Logger.debug("\t[CopyInto thread#%s] DONE duration: %s, for file: %s, size: %,d, rows: %,d", tableName, sw, file, file.length(), rows);
				}
				catch(Exception e)
				{
					Logger.error(e, "Error COPY INTO DATABASE, table: %s, file: %s, SQL: %s", tableName, file, sql);
				}			
				latch.countDown();
			};			
			
			
		}.start();
	}
		
	

	private Connection beginTransaction(DBConnection db) {
		return new Sql2o(db.dbUrl, db.dbUser, db.dbPass, QuirksDetector.forURL(db.dbUrl)).beginTransaction();
	}
	
	

}
