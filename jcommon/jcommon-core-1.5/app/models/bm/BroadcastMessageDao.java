package models.bm;

import java.util.Date;
import java.util.List;

import models.jcommon.util.DateUtil;

public class BroadcastMessageDao {

	public static List<BroadcastMessage> listByRole(String role)
	{
		role="%,"+role + ",%";
		Date endDate=DateUtil.newDate();
		return BroadcastMessage.find("roles_ like ? and (end_date == null or end_date <= ? ) order by order_", role, endDate).fetch();
	}
}
