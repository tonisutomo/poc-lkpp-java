package plugin;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;

import play.Logger;
import play.Play;
import play.PlayPlugin;

/**INformasi tentang kapan build -> precompiled
 * @author andikyulianto@yahoo.com
 */
public class BuildInfoPlugin extends PlayPlugin {

	
//	@Override
	public boolean compileSources() {
		File file=new File(Play.applicationPath + "/precompiled/java");
		String now = new SimpleDateFormat("yyyy.MMdd.HH").format(new Date(file.lastModified()));
		try {
			String buildInfo=String.format("Build#:%s\tJDK:%s\tOS:%s\tUSR:%s", 
					now,
					System.getProperty("java.version"), System.getProperty("os.name"), 
					System.getProperty("user.name"));
			Logger.info(buildInfo);
			FileUtils.write(new File(Play.applicationPath + "/precompiled/build"), buildInfo, "UTF-8");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

}
